﻿using System;
using System.Collections.Generic;
using System.Text;

namespace AwesomeApp.Models
{
    public class NoteModel
    {
        public string Filename { get; set; }
        public string Text { get; set; }
        public DateTime Date { get; set; }
    }
}
